package de.game.audio;



import de.game.models.IModelObserver;

public class AudioObserver implements IModelObserver {

	@Override
	public void onComplete()  {
		
	}

	@Override
	public void onBossSpawn() {
		Audio.playSoundOnce("siren.wav");
		
	}
	
	public void playerDead()  {
		Audio.playSoundOnce("explosion.wav");
	}

	@Override
	public void onHit()  {
		Audio.playSoundOnce("bomb.wav");
		
	}
	
	@Override
	public void playTheme()  {
		Audio.playSoundOnce("theme2.wav");
		
	}
		

}
