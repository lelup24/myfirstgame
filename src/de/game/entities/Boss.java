package de.game.entities;

import java.util.Random;

import de.game.models.Model;

public class Boss implements Entity {
	private int x;
	private int y;
	private int w = 350;
	private int h = 250;
	private String imgUrl = "resources/bossmodel.png";
	private float speedY = 0;
	private float speedX = 3;
	private static boolean alive = false;
	double timer = 0;
	double laser = 0;
	int hp;
	
	public Boss(int x, int y, float speedY, int hp) {
		this.hp = hp;
		this.x = x;
		this.y = y;
		this.speedY = speedY;
	}
	
	

	@Override
	public Entity getEntity() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getX() {
		// TODO Auto-generated method stub
		return this.x;
	}

	@Override
	public void setX(int x) {
		this.x = x;
		
	}

	@Override
	public int getY() {
		// TODO Auto-generated method stub
		return this.y;
	}

	@Override
	public void setY(int y) {
		this.y = y;
		
	}

	@Override
	public void move(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(long elapsedTime) {
		if (hp <= 0) {
			alive = false;
		} else {
			alive = true;
		}

		Random random = new Random();
		double zahl = random.nextDouble();
		timer = timer + elapsedTime * zahl;
		if (timer > 250 && Boss.alive == true) {
			if (true) {
				shoot();
				timer = 0;
			}
		}
		
		laser = laser + elapsedTime * zahl;
		if (laser > 5000 && Boss.alive == true) {
			laser();
			laser = 0;
		}
		
		this.y = Math.round(this.y + elapsedTime * speedY);
	}
		

	public int getH() {
		return h;
	}

	public int getW() {
		return w;
	}

	public double getDrawPointOne() {
		return this.x-(this.w / 2);
	}

	public double getDrawPointTwo() {
		return this.y-(this.h / 2);
	}



	public void setW(int w) {
		this.w = w;
	}


	public void setH(int h) {
		this.h = h;
	}


	public String getImgUrl() {
		return imgUrl;
	}


	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}



	public void setSpeedY(float f) {
		this.speedY = f;
		
	}
	
	public float getSpeedY() {
		return this.speedY;
		
	}
	
	public void shoot() {
		Random random = new Random();
		int x = random.nextInt(375) + 1;
		
		Missile missile = new Missile(x, this.y, - 0.5f);
		Model.getMissiles().add(missile);
	}


	public void laser() {
		Laser laser = new Laser(this.x - 30, 150, - 0.08f, 20);
		Model.getLasers().add(laser);
	}

	public float getSpeedX() {
		return speedX;
	}



	public void setSpeedX(float speedX) {
		this.speedX = speedX;
	}



	public int getHp() {
		return hp;
	}



	public void setHp(int hp) {
		this.hp = hp;
	}



	public static boolean isAlive() {
		return alive;
	}



	public static void setAlive(boolean alive) {
		Boss.alive = alive;
	}


}
