package de.game.entities;

public interface Entity {

	Entity getEntity();

	int getX();
	void setX(int x);

	int getY();
	void setY(int y);
	
	void move(int x, int y);

	void update(long elapsedTime);

	void delete();
}
