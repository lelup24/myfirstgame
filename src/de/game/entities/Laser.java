package de.game.entities;


public class Laser implements Entity {
	
	private int x;
	private int y;
	private int w;
	private int h;
	private float speedY;
	private boolean isPlayer = false;
	private String imgUrl = "resources/laser.png";
	private boolean isFired;

	
	public boolean isFired() {
		return isFired;
	}

	public void setFired(boolean isFired) {
		this.isFired = isFired;
	}

	public Laser(int x, int y, float speedY, int h) {
		this.x = x;
		this.y = y;
		this.isFired = false;
		this.speedY = speedY;
	}

	public void update (long elapsedTime) {
		this.y = Math.round(this.y - elapsedTime * speedY);
		if ( this.h < 400) {
			this.h = this.h + 4;
		} else  {
			this.h  += 0;
		}
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getH() {
		return this.h;
	}

	public int getW() {
		return 50;
	}

	public double getDrawPointOne() {
		return this.x-(this.w / 2);
	}

	public double getDrawPointTwo() {
		return this.y-(this.h / 2);
	}

	@Override
	public Entity getEntity() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public void move(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}

	public void setSpeedY(float speedY) {
		this.speedY = speedY;
	}

	public boolean isPlayer() {
		return isPlayer;
	}

	public void setPlayer(boolean isPlayer) {
		this.isPlayer = isPlayer;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	
	

}
