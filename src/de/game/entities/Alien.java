package de.game.entities;

import java.util.Random;

import de.game.models.Model;

public class Alien implements Entity {
	private int x;
	private int y;
	private int w = 50;
	private int h = 50;
	private String imgUrl = "resources/ufo2.png";
	private boolean isAlive = true;
	private boolean spawnable;
	private float speedY;
	private float speedX = 3;
	double timer = 0;
	
	public Alien(int x, int y, float speedY) {
		this.x = x;
		this.y = y;
		this.speedY = speedY;
	}
	
	

	@Override
	public Entity getEntity() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getX() {
		// TODO Auto-generated method stub
		return this.x;
	}

	@Override
	public void setX(int x) {
		this.x = x;
		
	}

	@Override
	public int getY() {
		// TODO Auto-generated method stub
		return this.y;
	}

	@Override
	public void setY(int y) {
		this.y = y;
		
	}

	@Override
	public void move(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(long elapsedTime) {
		
		Random random = new Random();
		double zahl = random.nextInt(100);
		timer = timer + elapsedTime * zahl / 200;
		if (timer > 150) {
			shoot();
			timer = 0;
		}
		
		this.x = Math.round(this.x + speedX);
	}
		

	public int getH() {
		return 50;
	}

	public int getW() {
		return 50;
	}

	public double getDrawPointOne() {
		return this.x-(this.w / 2);
	}

	public double getDrawPointTwo() {
		return this.y-(this.h / 2);
	}



	public void setW(int w) {
		this.w = w;
	}


	public void setH(int h) {
		this.h = h;
	}


	public String getImgUrl() {
		return imgUrl;
	}


	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	public boolean isAlive() {
		return isAlive;
	}


	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}



	public void setSpeedY(float f) {
		this.speedY = f;
		
	}
	
	public float getSpeedY() {
		return this.speedY;
		
	}
	
	public void shoot() {
		Missile missile = new Missile(this.x, this.y, - 0.5f);
		Model.getMissiles().add(missile);
	}



	public float getSpeedX() {
		return speedX;
	}



	public void setSpeedX(float speedX) {
		this.speedX = speedX;
	}



	public boolean isSpawnable() {
		return spawnable;
	}



	public void setSpawnable(boolean spawnable) {
		this.spawnable = spawnable;
	}
	

}
