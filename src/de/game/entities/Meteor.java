package de.game.entities;
//--module-path E:\Downloads\javafx-sdk-11.0.2\lib --add-modules=javafx.controls

public class Meteor implements Entity {
	
	private int x;
	private int y;
	private int w;
	private int h;
	private float speedY;
	private String imgUrl = "resources/meteor.png";
	private boolean exists;

	
	public boolean isExists() {
		return exists;
	}

	public void setExists(boolean exists) {
		this.exists = exists;
	}

	public Meteor(int x, int y, float speedY) {
		this.x = x;
		this.y = y;
		this.exists = true;
		this.speedY = speedY;
	}

	public void update (long elapsedTime) {
		this.y = Math.round(this.y + elapsedTime * speedY);
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getH() {
		return 80;
	}

	public int getW() {
		return 80;
	}

	public double getDrawPointOne() {
		return this.x-(this.w / 2);
	}

	public double getDrawPointTwo() {
		return this.y-(this.h / 2);
	}

	@Override
	public Entity getEntity() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public void move(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}

	public void setSpeedY(float speedY) {
		this.speedY = speedY;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


}
