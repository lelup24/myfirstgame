package de.game.entities;

import de.game.models.Model;

public class Player implements Entity {
	
	private int x;
	private int y;
	private int w = 50;
	private int h = 50;
	private String imgUrl = "resources/raumschiff.png";
	private String imgHpUrl = "resources/raumschiff.png";
	private int highScore = 0;
	private boolean isAlive = true;
	private boolean canMove = true;
	private int hp = 5;

    private boolean moveUp;
    private boolean moveDown;
    private boolean moveRight;
    private boolean moveLeft;
    private boolean stationary;

	
	public Player(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean isMoveUp() {
        return moveUp;
    }

    public boolean isMoveDown() {
        return moveDown;
    }

    public boolean isMoveRight() {
        return moveRight;
    }

    public boolean isMoveLeft() {
        return moveLeft;
    }

    public boolean isStationary() {
        stationary = (!this.isMoveDown() && !this.isMoveUp() && !this.isMoveRight() && !this.isMoveLeft());
        return stationary;
    }

	
	public void move(int dx, int dy) {

	}
	
	public void shoot() {
		if (isAlive == true) {
			Missile missile = new Missile(this.x, this.y, 1);
			missile.setPlayer(true);
			Model.getMissiles().add(missile);
		}
	}

	public int getX() {
		return x;
	}


	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public double getDrawPointOne() {
		return this.x-(this.w / 2);
	}

	public double getDrawPointTwo() {
		return this.y-(this.h / 2);
	}

	@Override
	public Entity getEntity() {
		// TODO Auto-generated method stub
		return this;
	}

	 public void update(long elapsedTime) {
		if (canMove) {
			float speed = Math.round(elapsedTime/4);
	        if (moveUp && !moveDown) {
	            this.y -= speed;
	        }
	        if (moveDown && !moveUp) {
	            this.y += speed;
	        }
	        if (moveRight && !moveLeft) {
	            this.x += speed;
	        }
	        if (moveLeft && !moveRight) {
	            this.x -= speed;
	        }
        }
	 }

	 public void setMoveUp(boolean moveUp) {
	        this.moveUp = moveUp;
	    }

	    public void setMoveDown(boolean moveDown) {
	        this.moveDown = moveDown;
	    }

	    public void setMoveRight(boolean moveRight) {
	        this.moveRight = moveRight;
	    }

	    public void setMoveLeft(boolean moveLeft) {
	        this.moveLeft = moveLeft;
	    }


	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}

	public int getW() {
		return w;
	}

	public int getH() {
		return h;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public int getHighScore() {
		return highScore;
	}

	public void setHighScore(int highScore) {
		this.highScore = highScore;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}
	
	public void setCanMove() {
		this.canMove = false;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public String getImgHpUrl() {
		return imgHpUrl;
	}

	public void setImgHpUrl(String imgHpUrl) {
		this.imgHpUrl = imgHpUrl;
	}
	
	
	
}
