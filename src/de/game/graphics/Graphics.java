package de.game.graphics;

import de.game.entities.Alien;
import de.game.entities.Boss;
import de.game.entities.Laser;
import de.game.entities.Meteor;
import de.game.entities.Missile;
import de.game.mechanics.HighScore;
import de.game.models.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class Graphics {
	
	private Model model;
	private GraphicsContext gc;
	private Image img;
	private Image img2;
	private Image img3;
	private Image laserImg;
	private Image endImage;
	private Image bossImage;
	private Image hpImage;
	private Image highscore;
	private Image background;
	
	public Graphics(Model model, GraphicsContext gc) {
		super();
		this.model = model;
		this.gc = gc;
		
	}
	
	public synchronized void draw() {
		gc.clearRect(0, 0, Model.WIDTH, Model.HEIGTH);
		gc.setFill(Color.PALEVIOLETRED);
		
		// Background
			background = new Image("resources/backgroundBig.png");
			gc.drawImage(background, 
					0, 
					model.getBackgroundY(), 
					400,
					model.getBackgroundHeight());
		
	
		
		// HP-Anzeige
		hpImage = new Image(model.getPlayer().getImgHpUrl());
		gc.drawImage(hpImage, 
				300, 
				600, 
				90,
				90);
		
		
		// Spieleranzeige
		img = new Image(model.getPlayer().getImgUrl());
		gc.drawImage(img, model.getPlayer().getDrawPointOne(), 
				model.getPlayer().getDrawPointTwo(), 
				model.getPlayer().getW(),
				model.getPlayer().getH());
		
		
		// Meteoranzeige
		img2 = new Image("resources/meteor.png");
		for (Meteor meteor : this.model.getMeteors()) {
			gc.drawImage(img2,
					meteor.getDrawPointOne(),
					meteor.getDrawPointTwo(),
					meteor.getW(), 
					meteor.getH()
			);
		}
		
		//Missiles
		img3 = new Image("resources/missile.png");
		for (Missile missile : Model.getMissiles()) {
			gc.drawImage(img3,
					missile.getDrawPointOne(),
					missile.getDrawPointTwo(),
					missile.getW(), 
					missile.getH()
			);
		}
		
		// Laser
		laserImg = new Image("resources/laser.png");
		for (Laser laser : Model.getLasers()) {
			gc.drawImage(laserImg,
					laser.getDrawPointOne(),
					laser.getDrawPointTwo(),
					laser.getW(), 
					laser.getH()
			);
		}
		
		
		// Boss
		bossImage = new Image("resources/bossmodel.png");
		for(Boss boss : Model.getBosses()) {
		gc.drawImage(bossImage, boss.getDrawPointOne(),			
				boss.getDrawPointTwo(),
				boss.getW(), 
				boss.getH()
				);
		}
		
		// Gameoveranzeige
		endImage = new Image(model.getImgUrl());
		gc.drawImage(endImage, 
				50, 
				20, 
				300,
				150);
		
		
		// Anzeige Aliens
		Image alienImg = new Image(this.model.getAlien().getImgUrl());
		for (Alien alien : Model.getAliens()) {
			gc.drawImage(alienImg,
					alien.getDrawPointOne(),
					alien.getDrawPointTwo(),
					alien.getW(), 
					alien.getH()
			);
		}
		
		
		// Highscore
			highscore = new Image(HighScore.getHighScoreImg());
			gc.drawImage(highscore, 
					76, 
					300, 
					250,
					250);

	}
	
}
