package de.game.models;

import java.util.*;

import de.game.entities.Alien;
import de.game.entities.Boss;
import de.game.entities.Entity;
import de.game.entities.Laser;
import de.game.entities.Meteor;
import de.game.entities.Missile;
import de.game.entities.Player;
import de.game.mechanics.Collision;


public class Model {
	
	public static final int WIDTH = 400;
	public static final int HEIGTH = 700;
	private double timer = 0;
	private double timer2 = 0;
	private static double faktor = 1;
	private static double faktor2 = 1;
	private static int level = 1;
	
	private static List<Meteor> meteors = new LinkedList<>();
	private static List<Alien> aliens = new LinkedList<>();
	private static List<Missile> missiles = new LinkedList<Missile>();
	private static List<Boss> bosses = new LinkedList<Boss>();
	private static List<Laser> lasers = new LinkedList<Laser>();
	private Player player;
	
	private String imgUrl = "resources/empty.png";
	private Alien alien;
	private Boss boss;
	private int backgroundHeight = 10000;
	private int backgroundY = -9300;
	int bossHp = 30;
	
	private List<IModelObserver> modelObservers = new LinkedList<IModelObserver>();


	public Model() {
		System.out.println("Game is running... \nHave Fun!");
		generateMeteorits();
		generateAliens();
		this.player = new Player(WIDTH/2, HEIGTH - 35);
		this.alien = new Alien(200, 0, 1);
	}
	
	
	public void addObserver(IModelObserver observer) {
		this.modelObservers.add(observer);
	}
	
	// Refreshed die Modelle und ermöglich per elapsedTime Erstellung zeitlicher Abläufe
	public synchronized void update(long elapsedTime) {
		
		if (backgroundY > 0) {
			backgroundY = -800;
		}
		if (player.isMoveUp()) {
			backgroundY = backgroundY + 7;
			backgroundHeight = backgroundHeight + 10;
		}
		else if (player.isMoveDown()) {
			backgroundY = backgroundY - 5;
			backgroundHeight = backgroundHeight - 10;
		}
		else {
			backgroundY = backgroundY + 5;
		}
		
		
		// Aktualisierung der Spieler-HP-Anzeige
		switch (player.getHp()) {
		case 5:
			player.setImgHpUrl("resources/schild5.png");
			break;
		case 4:
			player.setImgHpUrl("resources/schild4.png");
			break;
		case 3:
			player.setImgHpUrl("resources/schild3.png");
			break;
		case 2:
			player.setImgHpUrl("resources/schild2.png");
			break;
		case 1:
			player.setImgHpUrl("resources/schild1.png");
			break;
		case 0:
			player.setAlive(false);
			player.setImgUrl("resources/explosion.png");
			player.setCanMove();
			break;

		default:
			break;
		}
		
		player.update(elapsedTime);
		
		// Boss spawn
		if ((level == 5) || (level == 10) || (level == 15) || (level == 20) && bosses.size() <= 0) {
			Boss boss = new Boss(WIDTH/2, -50, 0.03f, bossHp);
			Boss.setAlive(true);
			bosses.add(boss);
			boss.setImgUrl("resources/boss.png");
			for (IModelObserver modelObserver : modelObservers) {
				modelObserver.onBossSpawn();
			}
			
			
		} else if (Boss.isAlive() == false) {
			imgUrl = "resources/empty.png";
		}
		
		for (IModelObserver modelObserver : modelObservers) {
			// Meteorabrufe
			for (Meteor entity : meteors) {
				if (player.isMoveUp()) {
					entity.setY(entity.getY() + 10);
				}
				else if (player.isMoveDown()) {
					entity.setY(entity.getY() - 20);
				}
				else {
					
				}
				Collision.enemyCollision((Meteor)entity, player, modelObserver);
				Collision.borderCollision(entity, player);
				Collision.borderCollision(player);
				entity.update(elapsedTime);
			}
			
			
			// Alienabrufe
			for (Alien entity : aliens) {
				Collision.enemyCollision((Alien)entity, player, modelObserver);
				Collision.borderCollision(entity, player, modelObserver);
				Collision.borderCollision(player);
				entity.update(elapsedTime);
				if (entity.isAlive() ==  false) {
					entity = null;
				}
				
			}
			
			// Missileabrufe
			for (Missile entity : missiles) {
				Collision.fireCollision(player, entity, modelObserver);
				for (Alien alien : aliens) {
					Collision.fireCollision(alien, entity, player, modelObserver);
				}
				for (Boss boss : bosses) {
					Collision.fireCollision(boss, entity, player, modelObserver);
				}
				
				entity.update(elapsedTime);	
			}
			
			
			// Bossabrufe
			for (Boss entity : bosses) {
				Collision.borderCollision(entity);
				entity.update(elapsedTime);	
			}
			
			
			// Laserabrufe
			for (Laser entity : lasers) {
				Collision.fireCollision(player, entity, modelObserver);
				entity.update(elapsedTime);	
			}
		}
		
		
		// Steuer die Generierung der Objekte
		creationTimer(elapsedTime);
		faktor += 1;
		level = (int) faktor2;
		if (player.isAlive() == false) {
			imgUrl = "resources/gameover.png";
		}
		
		
		// Löschen der Objekte nach deren Verwendung
		meteors.removeIf(meteor -> meteor.isExists() == false);
		missiles.removeIf(missile -> missile.isFired() == true);
		aliens.removeIf(aliens -> aliens.isAlive() == false);
		lasers.removeIf(lasers -> lasers.isFired() == true);
		lasers.removeIf(lasers -> lasers.getY() > 800);
		missiles.removeIf(missile -> missile.getY() > 800);
		bosses.removeIf(boss -> boss.getHp() <= 0);

	}
	
	
	// erstellt Meteoriten
	public static void generateMeteorits() {
		Random random = new Random();
		
		for (int i = 0; i < faktor2 ; i++) {
			int x = random.nextInt(375) + 1;
			float v = (float) ((random.nextFloat() / 1000 * 250) + 0.1f);
			meteors.add(new Meteor(x, -1500, v));
		} faktor2 *= 1.05;
	}
	
	
	// erstellt Aliens
	public static void generateAliens() {
		Random random = new Random();
	
		int x = random.nextInt(375) + 1;
		double acceleration = faktor/10000;
		float v = (float) (((random.nextFloat() + acceleration) / 1000 * 250) + 0.1f);
		
		aliens.add(new Alien(x, 50, v));
	}
	
	// setzt die Zeit, wann Objekte erstellt werden
	private void creationTimer(long elapsedTime) {
		
		timer += elapsedTime * 1500 + (faktor / 1000);
		timer2 += elapsedTime * 1500 + (faktor / 1000);
		
		if (timer > 5000000 && player.isAlive()) {
			generateMeteorits();
			timer = faktor;
		}
		
		if (timer2 > 15000000 && player.isAlive()) {
			generateAliens();
			timer2 = faktor;
		} 
		
	}
	
	public static List<Laser> getLasers() {
		return lasers;
	}

	public static void setLasers(List<Laser> lasers) {
		Model.lasers = lasers;
	}
	
	public static List<Missile> getMissiles() {
		return Model.missiles;
	}

	
	public List<Meteor> getMeteors() {
		return Model.meteors;
		
	}
	
	public Boss getBoss() {
		return boss;
	}


	public void setBoss(Boss boss) {
		this.boss = boss;
	}
	
	public Player getPlayer() {
		return this.player;
	}
	
	public Alien getAlien() {
		return this.alien;
	}
	
	
	public static List<Boss> getBosses() {
		return bosses;
	}


	public void delete(Entity entity) {
		this.getMeteors().remove(entity);
	}
	
	public synchronized static void addEntity(Meteor meteor) {
		meteors.add(meteor);
	}


	public static int getLevel() {
		return level;
	}


	public static void setLevel(int level) {
		Model.level = level;
	}
	
	public int setXEnd() {
		if(!player.isAlive()) {
			return 80;
		} else {
			return -80;
		}
	}


	public String getImgUrl() {
		return imgUrl;
	}


	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	public static List<Alien> getAliens() {
		return aliens;
	}


	public static void setAliens(List<Alien> aliens) {
		Model.aliens = aliens;
	}


	public static void setMissiles(List<Missile> missiles) {
		Model.missiles = missiles;
	}
	
	
	public List<IModelObserver> getModelObservers() {
		return modelObservers;
	}


	public void setModelObservers(List<IModelObserver> modelObservers) {
		this.modelObservers = modelObservers;
	}


	public int getBackgroundHeight() {
		return backgroundHeight;
	}


	public void setBackgroundHeight(int backgroundHeight) {
		this.backgroundHeight = backgroundHeight;
	}


	public int getBackgroundY() {
		return backgroundY;
	}


	public void setBackgroundY(int backgroundY) {
		this.backgroundY = backgroundY;
	}
	
	
	
}
