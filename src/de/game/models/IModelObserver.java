package de.game.models;


public interface IModelObserver {
	
	void onComplete();
	void onBossSpawn();
	void playerDead();
	void onHit();
	void playTheme();

}
