package de.game.game;


import de.game.mechanics.HighScore;
import de.game.mechanics.InputHandler;
import de.game.mechanics.Timer;
import de.game.models.IModelObserver;
import de.game.models.Model;
import de.game.audio.AudioObserver;
import de.game.graphics.Graphics;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Start extends Application {
	
	private Timer timer;


	@Override
	public void start(Stage stage) throws Exception {
		
		IModelObserver audioObserver = new AudioObserver();
		HighScore alterHighScore = new HighScore();
		
		
				
		Label highScore = new Label("Highscore");
		highScore.setTranslateX(10);
		highScore.setTranslateY(10);
		highScore.setFont(new Font("Arial", 30));
		highScore.setTextFill(Color.WHITE);
		
		
		Label level = new Label("Level");
		level.setTranslateX(Model.WIDTH/2 - 50);
		level.setTranslateY(10);
		level.setFont(new Font("Arial", 30));
		level.setTextFill(Color.WHITE);
		
		
		Label highScoreOld = new Label(String.valueOf(alterHighScore.getHighScore()));
		highScoreOld.setTranslateX(Model.WIDTH - 100);
		highScoreOld.setTranslateY(10);
		highScoreOld.setFont(new Font("Arial", 30));
		highScoreOld.setTextFill(Color.WHITE);
			
		
//        Image imgBackground = new Image("resources/background.jpg");
//        BackgroundImage backgroundImage = new BackgroundImage(
//                imgBackground,
//                BackgroundRepeat.REPEAT,
//                BackgroundRepeat.REPEAT,
//                BackgroundPosition.CENTER,
//                BackgroundSize.DEFAULT);
//        Background background = new Background(backgroundImage);
        
        
		Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGTH);
	

		GraphicsContext gc = canvas.getGraphicsContext2D();
		
		Model model = new Model();
		model.addObserver(audioObserver);
		Graphics graphics = new Graphics(model, gc);
		
		Pane group = new Pane();
//		group.setBackground(background);
		group.getChildren().add(canvas);
		group.getChildren().add(highScore);
		group.getChildren().add(level);
		group.getChildren().add(highScoreOld);
		

		Scene scene = new Scene(group);
		
	
		stage.setScene(scene);
		stage.show();
		audioObserver.playTheme();
	
		timer = new Timer(model, graphics);
		timer.start();
		
		InputHandler inputHandler = new InputHandler(model);
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				inputHandler.onKeyPressed(event.getCode());
				
			}
		});
		//canvas.setOnMouseClicked(event->event.x);
		scene.setOnKeyReleased(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				inputHandler.onKeyReleased(event.getCode());
				highScore.setText(String.valueOf(model.getPlayer().getHighScore()));
				level.setText("Level " + String.valueOf(Model.getLevel()));	
			}
		});

}

		
	
	
	@Override
	public void stop() throws Exception{
		timer.stop();
		super.stop();
	}
	
	public static void main(String[] args) {
		launch(args);		
		
	}

}
	
