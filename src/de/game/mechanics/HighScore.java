package de.game.mechanics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class HighScore {
	private static int highScore;
	private static String highScoreImg = "resources/empty.png";
	static String userName = System.getProperty("user.name");
	private static String fileName= "highscore.txt";
	
	public HighScore() {
		this.highScore = readTxt();
	}

	public static String createFileName() {
		return "C:/Users/" + userName + "/documents/" + fileName;
		
	}
	
	public int getHighScore() {
		return highScore;
	}

	public void setHighScore(int highScore) {
		this.highScore = highScore;
	}
	
	

	public static void setHighScoreImg(String highScoreImg) {
		HighScore.highScoreImg = highScoreImg;
	}

	public static void newHighScore(int playerScore) {
		if (playerScore > readTxt()) {
			highScore = playerScore;
			
			File datei = new File(createFileName());

			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(datei));
				bw.write(String.valueOf(highScore));
				highScoreImg = "resources/highscore.png";
				bw.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public static int readTxt() {
		File file = new File(createFileName());
		if (!file.exists()) {
			try {
				file.createNewFile();
				System.out.println("Datei für Highscores erstellt in " + createFileName());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		try {

			BufferedReader br = new BufferedReader(new FileReader(createFileName()));

			String line;
			while ((line = br.readLine()) != null) {
				String[] words = line.split(" ");
				for (String string : words) {
					highScore = Integer.parseInt(string);
				}
			}
			br.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return highScore;

	}

	public static String getHighScoreImg() {
		// TODO Auto-generated method stub
		return highScoreImg;
	}
	
	
	

}

