package de.game.mechanics;

import de.game.models.Model;


import de.game.graphics.Graphics;
import javafx.animation.AnimationTimer;

public class Timer extends AnimationTimer {
	private Model model;
	private Graphics graphics;
	private long previousTime = -1;
	
	
	public Timer(Model model, Graphics graphics) {
		super();
		this.model = model;
		this.graphics = graphics;
	}

	@Override
	public synchronized void handle(long nowNano) {
		long nowMillis = nowNano / 1000000;
		long elapsedTime;
		if (previousTime == -1) {
			elapsedTime = 0;
		} else {
			elapsedTime = nowMillis - previousTime;
		}
		previousTime = nowMillis;
		
		model.update(elapsedTime);
		graphics.draw();
	
	}

}
