package de.game.mechanics;

import de.game.game.Start;
import de.game.models.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {

	private Model model;

	public InputHandler(Model model) {
		this.model = model;
	}

	public void onKeyPressed(KeyCode key) {

		switch (key) {
		case W:
			model.getPlayer().setMoveUp(true);
			break;
		case S:
			model.getPlayer().setMoveDown(true);
			break;
		case A:
			model.getPlayer().setMoveLeft(true);
			break;
		case D:
			model.getPlayer().setMoveRight(true);
			break;
		case SPACE:
			model.getPlayer().shoot();
		case ENTER:
			
		default:
			break;
		}
	}

	public void onKeyReleased(KeyCode key) {
		switch (key) {
		case W:
			model.getPlayer().setMoveUp(false);
			break;
		case S:
			model.getPlayer().setMoveDown(false);
			break;
		case A:
			model.getPlayer().setMoveLeft(false);
			break;
		case D:
			model.getPlayer().setMoveRight(false);
			break;
		default:
			break;
		}
	}
}
