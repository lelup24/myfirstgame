package de.game.mechanics;

import de.game.audio.AudioObserver;
import de.game.entities.Alien;
import de.game.entities.Boss;
import de.game.entities.Laser;
import de.game.entities.Meteor;
import de.game.entities.Missile;
import de.game.entities.Player;
import de.game.models.IModelObserver;
import de.game.models.Model;

public class Collision {
	

	static AudioObserver audioObserver = new AudioObserver();
	
	public Collision() {
		
	}
	
	public static void borderCollision(Player player) {
		
		if (player.getX() - 15 < 0) {
;
			player.setX(25);
		}
		else if (player.getX() > Model.WIDTH -15) {

			player.setX(Model.WIDTH - 25);
		} 
		else if (player.getY() - 15 < 0) {

			player.setY(25);
		}
		else if (player.getY() > Model.HEIGTH - 15) {

			player.setY(Model.HEIGTH - 25);
		}

	}
	
	public static void borderCollision(Meteor meteor, Player player) {
		
		
		if (meteor.getY() > 700) {
			meteor.setExists(false);
			player.setHighScore(player.getHighScore() + 100);
		}
		
	}
	
	public static void borderCollision(Missile missile) {
		
		
		if (missile.getY() < 0) {
			missile.setFired(true);

		}
	}
	
	
	public static void borderCollision(Alien alien, Player player, IModelObserver modelObserver) {
		
		if (alien.getY() > 700) {
			alien.setAlive(false);
		}
		if (alien.getX() > 380) {
			alien.setX(360);
			alien.setSpeedX(alien.getSpeedX() * (-1));
			alien.setY(alien.getY() + 50);
		}
		
		if (alien.getX() < 20) {
			alien.setX(40);
			alien.setSpeedX(alien.getSpeedX() * (-1));
			alien.setY(alien.getY() + 50);
		}
	}
	
	public static void borderCollision(Boss boss) {
			
			if (boss.getY() > 100) {
				boss.setSpeedY(0);
			}
		}
	
	public static void enemyCollision(Meteor meteor, Player player, IModelObserver modelObserver)  {
			if(Math.abs(player.getDrawPointTwo() - meteor.getDrawPointTwo()) < 40 && 
					Math.abs((player.getDrawPointOne() - 10) - meteor.getDrawPointOne()) < 35) {
				meteor.setX(-50);
				meteor.setY(-50);
				meteor.setSpeedY(0);
				meteor.setExists(false);
				meteor = null;
				player.setAlive(false);
				playerDead(player, modelObserver);
				
			}			
			
	}

	
	public static void enemyCollision(Alien alien, Player player, IModelObserver modelObserver) {
			if(Math.abs(player.getDrawPointTwo() - alien.getDrawPointTwo()) < 40 && 
					Math.abs((player.getDrawPointOne() - 10) - alien.getDrawPointOne()) < 35) {
				alien.setX(-50);
				alien.setY(-50);
				alien.setSpeedY(0);
				player.setAlive(false);
				playerDead(player, modelObserver);
				alien.setAlive(false);
			}	
	}
	
	public static void fireCollision(Alien alien, Missile missile, Player player, IModelObserver modelObserver)  {
		if (missile.isPlayer()) {
			if(Math.abs(missile.getDrawPointTwo() - alien.getDrawPointTwo()) < 30 && 
					Math.abs((missile.getDrawPointOne() - 10) - alien.getDrawPointOne()) < 30) {
				player.setHighScore(player.getHighScore() + 500);
				alien.setAlive(false);
				audioObserver.onHit();
				alien.setSpeedY(0);
				missile.setFired(true);
			}
		}	
	}
	
	public static void fireCollision(Player player, Missile missile, IModelObserver modelObserver) {
		if (!missile.isPlayer()) {
			if(Math.abs(missile.getDrawPointTwo() - player.getDrawPointTwo()) < 20 && 
					Math.abs((missile.getDrawPointOne() - 10) - player.getDrawPointOne()) < 20) {
				audioObserver.onHit();
				missile.setPlayer(true);
				player.setHp(player.getHp() - 1);
				missile.setFired(true);
			}

		}	
	}
	
	public static void fireCollision(Player player, Laser laser, IModelObserver modelObserver)  {
			if(Math.abs(laser.getDrawPointTwo() - player.getDrawPointTwo()) < laser.getH() && 
					Math.abs((laser.getDrawPointOne() + 20) - player.getDrawPointOne()) < 40) {
				player.setAlive(false);
				playerDead(player, modelObserver);

		}	
	}
	
	public static void fireCollision(Boss boss, Missile missile, Player player, IModelObserver modelObserver) {
		if (missile.isPlayer()) {
			if(Math.abs(missile.getDrawPointTwo() - boss.getDrawPointTwo()) < 300 && 
					Math.abs((missile.getDrawPointOne() - 20) - boss.getDrawPointOne()) < 150) {
				
				audioObserver.onHit();
				boss.setHp(boss.getHp() - 1);
				boss.setAlive(false);
				if (boss.isAlive() == false) {
					boss.setX(-500);
					boss.setY(-500);
					boss.setSpeedY(0);
					player.setHighScore(player.getHighScore() + 20000);
				}	
				missile.setFired(true);
			}
		}	
	}
	
	private static void playerDead(Player player, IModelObserver modelObserver){
		if (!player.isAlive()) {
			player.setImgUrl("resources/explosion.png");
			audioObserver.playerDead();
			HighScore.newHighScore(player.getHighScore());
			player.setCanMove();
		}
	}
}
